﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vjezba.Web.Models.Mock
{
    public class Company
    {
        public int ID { get; set; }
        [Required]
        [StringLength(1000, MinimumLength = 5, ErrorMessage = "Naziv mora imati najmanje 5 znakova")]
        public string Name { get; set; }

        [Required]
        [StringLength(1000, MinimumLength = 5, ErrorMessage = "E-mail adresa mora imati najmanje 5 znakova")]
        public string Email { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [Range(-90.00, 90.00, ErrorMessage = "Geografska širina mora biti između -90.00 i 90.00")]
        public decimal Latitude { get; set; }

        [Required]
        [Range(-180.00, 180.00, ErrorMessage = "Geografska dužina mora biti između -180.00 i 180.00")]
        public decimal Longitude { get; set; }

        public DateTime DateFrom { get; set; }

        public int CityID { get; set; }

        [Required]
        public City City { get; set; }

    }

}