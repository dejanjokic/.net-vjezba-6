﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Web.Models;
using Vjezba.Web.Models.Mock;

namespace Vjezba.Web.Controllers
{
    [RoutePrefix("komp")]
    public class CompanyController : Controller
    {
        public ActionResult Index(string query)
        {
            var companiesQuery = MockCompanyRepository.GetInstance().All();

            if (!string.IsNullOrWhiteSpace(query))
                companiesQuery = companiesQuery.Where(p => p.Name.Contains(query));

            var model = companiesQuery.ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(string queryName, string queryAddress)
        {
            var companiesQuery = MockCompanyRepository.GetInstance().All();

            //Primjer iterativnog građenja upita - dodaje se "where clause" samo u slučaju da je parametar doista proslijeđen.
            //To rezultira optimalnijim stablom izraza koje se kvalitetnije potencijalno prevodi u SQL
            if (!string.IsNullOrWhiteSpace(queryName))
                companiesQuery = companiesQuery.Where(p => p.Name.Contains(queryName));

            if (!string.IsNullOrWhiteSpace(queryAddress))
                companiesQuery = companiesQuery.Where(p => p.Address.Contains(queryAddress));

            var model = companiesQuery.ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult AdvancedSearch(CompanyFilterModel model)
        {
            var companiesQuery = MockCompanyRepository.GetInstance().All();

            if (!string.IsNullOrWhiteSpace(model.Name))
                companiesQuery = companiesQuery.Where(p => p.Name.Contains(model.Name));

            if (!string.IsNullOrWhiteSpace(model.Address))
                companiesQuery = companiesQuery.Where(p => p.Address.Contains(model.Address));

            if (!string.IsNullOrWhiteSpace(model.Email))
                companiesQuery = companiesQuery.Where(p => p.Email.Contains(model.Email));

            if (!string.IsNullOrWhiteSpace(model.CityName))
                companiesQuery = companiesQuery.Where(p => p.City.Name.Contains(model.CityName));

            var data = companiesQuery.ToList();

            //Iskoristit ćemo postojeći view Index. Potrebno je eksplicitno reći koji view želimo renderirati
            //jer se inače pokušava pronaći template AdvancedSearch.cshtml
            return View("Index", data);
        }

        public ActionResult Details(int? id = null)
        {
            if (id == null)
                return View();

            var model = MockCompanyRepository.GetInstance().FindByID(id.Value);

            return View(model);
        }

        [Route("pretraga/{q:alpha:length(2,5)}")]
        public ActionResult Search(string q)
        {
            //Kada se bude radilo s bazom podataka, ova metoda još neće "otići" u bazu po podatke
            var query = MockCompanyRepository.GetInstance().All();

            if (!string.IsNullOrWhiteSpace(q))
                query = query.Where(p => p.Name.Contains(q));

            //Kada se bude radilo s bazom podataka, tek ova metoda bi generirala ispravni SQL i izvršila upit na bazi
            //te vratila samo potrebne rezultate
            var model = query.ToList();

            return View("Index", model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Company model)
        {
            model.ID = MockCompanyRepository.GetInstance().All()
                .Max(p => p.ID) + 1;
            model.CityID = MockCityRepository.GetInstance().All()
                .Select(p => p.ID)
                .First();

            //Za sada ne koristimo Model.IsValid i ne koristimo UpdateModel() funkciju
            MockCompanyRepository.GetInstance().InsertOrUpdate(model);

            return RedirectToAction("Index");
        }

        [ActionName("Edit")]
        public ActionResult EditGet(int id)
        {
            Company company = MockCompanyRepository.GetInstance().FindByID(id);

            return View("Edit", company);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditPost(int id)
        {
            var isUpdated = this.TryUpdateModel(MockCompanyRepository.GetInstance().FindByID(id));

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            if (isUpdated && ModelState.IsValid)
            {
                return RedirectToAction("Index", "Company");
            }
            return View(MockCompanyRepository.GetInstance().FindByID(id));
        }
    }
}